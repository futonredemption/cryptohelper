Crypto Helpers
==============

Helpers for things like generating certificates.

There's 2 modes:
 - Simple HTTPS certificates
 - Issue HTTPS certificates from a Cert Authority
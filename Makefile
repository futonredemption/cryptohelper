GO := @go
GOGET := go get -u -d
GOGETBUILD := go get -u
SOURCE_DIRS=$(shell go list ./... | grep -v '/vendor/')
export PATH := $(PATH):/usr/local/go/bin:/usr/go/bin

all: build
build: httpscert

httpscert:
	$(GO) build tools/httpscert.go

lint:
	$(GO) fmt ${SOURCE_DIRS}
	$(GO) vet ${SOURCE_DIRS}

clean:
	@rm -f httpscert *.pem coverage.txt

check: test

test:
	$(GO) test -cover ${SOURCE_DIRS} -race

coverage.txt:
	$(GO) test -cover ${SOURCE_DIRS} -coverprofile=$(PWD)/coverage.txt -covermode count ./

bench: benchmark

benchmark:
	$(GO) test -cover -benchmem -bench=. ${SOURCE_DIRS}
	
deps:
	$(GOGETBUILD) github.com/t-yuki/gocover-cobertura
	$(GOGET) github.com/stretchr/testify/...

.PHONY : all build lint clean check test bench benchmark deps

package main

import (
	"bitbucket.org/futonredemption/cryptohelper/cert"
	"flag"
	"fmt"
	"log"
	"os"
	"path/filepath"
)

var keyType = flag.String("type", "https", "Type of key to generate.")

var httpsName = flag.String("https.name", "server", "The base name of the HTTPs certificate.")
var caName = flag.String("ca.name", "root", "The base name of the root certificate.")
var caIssueName = flag.String("ca.issuename", "server", "The base name of the issued certificates.")
var caIssue = flag.Int("ca.issue", 1, "Number of issued certificates to generate.")

func main() {
	flag.Parse()
	switch *keyType {
	case "https":
		makeHttpsCertificate()
	case "ca":
		makeCertificateAuthorityCertificates()
	default:
		log.Fatalf("--type %s is not supported.", *keyType)
	}
}

func makeHttpsCertificate() {
	pair, err := cert.AsHttpsCertificate(cert.NewInfo())
	check(err)
	pwd, err := os.Getwd()
	check(err)
	check(pair.Write(filepath.Join(pwd, *httpsName+".cert.pem"), filepath.Join(pwd, *httpsName+".rsa.pem")))
}

func makeCertificateAuthorityCertificates() {
	pair, err := cert.AsCertificateAuthority(cert.NewInfo())
	check(err)
	pwd, err := os.Getwd()
	check(err)
	check(pair.Write(filepath.Join(pwd, *caName+".cert.pem"), filepath.Join(pwd, *caName+".key.pem")))
	for i := 0; i < *caIssue; i++ {
		issuedPair, err := cert.IssueFrom(pair, cert.NewInfo())
		check(err)
		idx := fmt.Sprintf(".%d", i)
		check(issuedPair.Write(filepath.Join(pwd, *caIssueName+idx+".cert.pem"), filepath.Join(pwd, *caIssueName+idx+".rsa.pem")))
	}
}

func check(err error) {
	if err != nil {
		log.Fatalf("Error creating certificates: %v", err)
	}
}

package cert

import (
	"crypto/rand"
	"crypto/x509"
)

func AsCertificateAuthority(info *Info) (*CertificateKeyPair, error) {
	privateKey, err := createPrivateKey(info.kd)
	if err != nil {
		return nil, err
	}

	template, err := createCertificateTemplate(info)
	if err != nil {
		return nil, err
	}

	template.IsCA = true
	template.KeyUsage |= x509.KeyUsageCertSign

	derBytes, err := x509.CreateCertificate(rand.Reader, template, template, publicKey(privateKey), privateKey)
	if err != nil {
		return nil, err
	}

	pemPriv, err := pemBlockForKey(privateKey)
	if err != nil {
		return nil, err
	}

	return newCertificateKeyPair(newCertificatePemBlock(derBytes), pemPriv), nil
}

func IssueFrom(pair *CertificateKeyPair, info *Info) (*CertificateKeyPair, error) {
	privateKey, err := createPrivateKey(info.kd)
	if err != nil {
		return nil, err
	}

	template, err := createCertificateTemplate(info)
	if err != nil {
		return nil, err
	}

	reinflatedRootCa, err := rawPemToPublicCertificate(pair.X509PEMBytes)
	if err != nil {
		return nil, err
	}

	template.SerialNumber = reinflatedRootCa.SerialNumber
	template.Issuer = reinflatedRootCa.Issuer

	caPrivateKey, err := rawPemToPrivateKey(pair.PrivateKeyPEMBytes)
	if err != nil {
		return nil, err
	}

	derBytes, err := x509.CreateCertificate(rand.Reader, template, reinflatedRootCa, publicKey(privateKey), caPrivateKey)
	if err != nil {
		return nil, err
	}

	pemPriv, err := pemBlockForKey(privateKey)
	if err != nil {
		return nil, err
	}

	return newCertificateKeyPair(newCertificatePemBlock(derBytes), pemPriv), nil
}

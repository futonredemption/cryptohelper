package cert

import (
	"fmt"
	"io/ioutil"
)

type CertificateKeyPair struct {
	X509PEMBytes       []byte
	PrivateKeyPEMBytes []byte
}

// Attempts to load the certificate key pair
func ReadCertificateKeyPairFromFile(pubPath string, privPath string) (*CertificateKeyPair, error) {
	if fileExists(pubPath) && fileExists(privPath) {
		pubPem, err := ioutil.ReadFile(pubPath)
		if err != nil {
			return nil, err
		}
		privPem, err := ioutil.ReadFile(privPath)
		if err != nil {
			return nil, err
		}
		return &CertificateKeyPair{
			X509PEMBytes:       pubPem,
			PrivateKeyPEMBytes: privPem,
		}, nil
	}
	if fileExists(pubPath) {
		return nil, fmt.Errorf("Public key path %s exists but private key does not, %s", pubPath, privPath)
	} else if fileExists(privPath) {
		return nil, fmt.Errorf("Private key path %s exists but public key does not, %s", privPath, pubPath)
	}
	return nil, nil
}

func NewCertificateKeyPair(publicPemPath, privatePemPath string) (*CertificateKeyPair, error) {
	publicPem, err := ioutil.ReadFile(publicPemPath)
	if err != nil {
		return nil, err
	}
	privatePem, err := ioutil.ReadFile(privatePemPath)
	if err != nil {
		return nil, err
	}
	return &CertificateKeyPair{
		X509PEMBytes:       publicPem,
		PrivateKeyPEMBytes: privatePem,
	}, nil
}

func (this *CertificateKeyPair) GetCertificatePEM() string {
	return string(this.X509PEMBytes)
}

func (this *CertificateKeyPair) WriteCertificate(path string) error {
	return ioutil.WriteFile(path, this.X509PEMBytes, 0664)
}

func (this *CertificateKeyPair) GetPrivateKeyPEM() string {
	return string(this.PrivateKeyPEMBytes)
}

func (this *CertificateKeyPair) WritePrivateKey(path string) error {
	return ioutil.WriteFile(path, this.PrivateKeyPEMBytes, 0660)
}

func (this *CertificateKeyPair) Write(certPath, keyPath string) error {
	err := this.WriteCertificate(certPath)
	if err != nil {
		return err
	}
	return this.WritePrivateKey(keyPath)
}

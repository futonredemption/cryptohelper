package cert

import (
	"github.com/stretchr/testify/assert"
	"os/user"
	"testing"
)

func TestInfoConstructor(t *testing.T) {
	assert := assert.New(t)
	info := NewInfo()
	assert.Equal("", info.Hosts)
	currentUser, err := user.Current()
	assert.Nil(err)
	assert.Equal(currentUser.Name, info.Organization)
	assert.Equal(currentUser.Username, info.OrganizationUnit)
	assert.Equal("US", info.Country)
	assert.Equal("Seattle", info.Locality)
	assert.Equal("Washington", info.Province)
	assert.Equal(2048, info.kd.rsaBits)
	assert.Nil(info.kd.ecdsaCurve)
}

func ExampleNewInfo() {
	AsHttpsCertificate(NewInfo())
}

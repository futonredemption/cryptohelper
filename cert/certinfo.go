package cert

import (
	"crypto/elliptic"
	"crypto/x509/pkix"
	"os/user"
	"time"
)

type Info struct {
	ValidDuration    time.Duration
	StartTime        time.Time
	Hosts            string
	Organization     string
	OrganizationUnit string
	Country          string
	Locality         string
	Province         string

	kd *keyDescriptor
}

// NewInfo returns reasonable defaults for creating a certificate.
func NewInfo() *Info {
	defaultCertOrg := "Developer"
	defaultCertOrgUnit := "Dev"
	currentUser, err := user.Current()
	if err == nil {
		defaultCertOrg = currentUser.Name
		defaultCertOrgUnit = currentUser.Username
	}
	return &Info{
		ValidDuration:    kOneYearDuration,
		StartTime:        time.Now(),
		Hosts:            "",
		Organization:     defaultCertOrg,
		OrganizationUnit: defaultCertOrgUnit,
		Country:          "US",
		Locality:         "Seattle",
		Province:         "Washington",
		kd: &keyDescriptor{
			rsaBits:    2048,
			ecdsaCurve: nil,
		},
	}
}

func (this *Info) UsingRsa1024() *Info {
	this.kd = &keyDescriptor{
		rsaBits:    1024,
		ecdsaCurve: nil,
	}
	return this
}

func (this *Info) UsingRsa2048() *Info {
	this.kd = &keyDescriptor{
		rsaBits:    2048,
		ecdsaCurve: nil,
	}
	return this
}

func (this *Info) UsingRsa4096() *Info {
	this.kd = &keyDescriptor{
		rsaBits:    4096,
		ecdsaCurve: nil,
	}
	return this
}

func (this *Info) UsingEcdsaP224() *Info {
	this.kd = &keyDescriptor{
		rsaBits:    0,
		ecdsaCurve: elliptic.P224(),
	}
	return this
}

func (this *Info) UsingEcdsaP256() *Info {
	this.kd = &keyDescriptor{
		rsaBits:    0,
		ecdsaCurve: elliptic.P256(),
	}
	return this
}

func (this *Info) UsingEcdsaP384() *Info {
	this.kd = &keyDescriptor{
		rsaBits:    0,
		ecdsaCurve: elliptic.P384(),
	}
	return this
}

func (this *Info) UsingEcdsaP521() *Info {
	this.kd = &keyDescriptor{
		rsaBits:    0,
		ecdsaCurve: elliptic.P521(),
	}
	return this
}

func (this *Info) toPkix() pkix.Name {
	return pkix.Name{
		Country:            []string{this.Country},
		Organization:       []string{this.Organization},
		OrganizationalUnit: []string{this.OrganizationUnit},
		Locality:           []string{this.Locality},
		Province:           []string{this.Province},
		CommonName:         this.Organization,
	}
}

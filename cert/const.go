package cert

import (
	"time"
)

const kOneYearDuration = time.Duration(time.Hour * 24 * 365 * 5)

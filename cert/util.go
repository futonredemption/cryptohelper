package cert

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"fmt"
	"math/big"
	"net"
	"os"
	"strings"
)

const kRsaPrivateKeyPemHeader = "RSA PRIVATE KEY"
const kEllipticalCurvePrivateKeyHeader = "EC PRIVATE KEY"
const kPublicCertificatePemHeader = "CERTIFICATE"

type keyDescriptor struct {
	rsaBits    int
	ecdsaCurve elliptic.Curve
}

func createPrivateKey(kd *keyDescriptor) (interface{}, error) {
	if kd.ecdsaCurve == nil {
		return rsa.GenerateKey(rand.Reader, kd.rsaBits)
	} else {
		return ecdsa.GenerateKey(kd.ecdsaCurve, rand.Reader)
	}
}

func publicKey(priv interface{}) interface{} {
	switch k := priv.(type) {
	case *rsa.PrivateKey:
		return &k.PublicKey
	case *ecdsa.PrivateKey:
		return &k.PublicKey
	default:
		return nil
	}
}

func rawPemToPublicCertificate(pemBlock []byte) (*x509.Certificate, error) {
	publicCertPemBlock, rest := pem.Decode(pemBlock)
	if len(rest) > 0 {
		return nil, fmt.Errorf("The PEM block for public certificate is not valid, %v", rest)
	}
	if publicCertPemBlock == nil {
		return nil, fmt.Errorf("PEM block decode was empty. %v", publicCertPemBlock)
	}
	return x509.ParseCertificate(publicCertPemBlock.Bytes)
}

func rawPemToPrivateKey(pemBlock []byte) (interface{}, error) {
	caPrivateKeyPemBlock, rest := pem.Decode(pemBlock)
	if len(rest) > 0 {
		return nil, fmt.Errorf("The PEM block for private key is not valid, %v", rest)
	}
	if caPrivateKeyPemBlock == nil {
		return nil, fmt.Errorf("PEM block decode was empty. %v", caPrivateKeyPemBlock)
	}
	if caPrivateKeyPemBlock.Type == kEllipticalCurvePrivateKeyHeader {
		return x509.ParseECPrivateKey(caPrivateKeyPemBlock.Bytes)
	} else if caPrivateKeyPemBlock.Type == kRsaPrivateKeyPemHeader {
		return x509.ParsePKCS1PrivateKey(caPrivateKeyPemBlock.Bytes)
	} else {
		return nil, fmt.Errorf("The PEM type is not supported, %s", caPrivateKeyPemBlock.Type)
	}
}

func pemBlockForKey(priv interface{}) (*pem.Block, error) {
	switch k := priv.(type) {
	case *rsa.PrivateKey:
		return &pem.Block{
			Type:  kRsaPrivateKeyPemHeader,
			Bytes: x509.MarshalPKCS1PrivateKey(k),
		}, nil
	case *ecdsa.PrivateKey:
		b, err := x509.MarshalECPrivateKey(k)
		if err != nil {
			return nil, err
		}
		return &pem.Block{
			Type:  kEllipticalCurvePrivateKeyHeader,
			Bytes: b,
		}, nil
	default:
		return nil, fmt.Errorf("Creating a pem.Block for type %v is not supported.", k)
	}
}

func newSerialNumber() (*big.Int, error) {
	serialNumberLimit := new(big.Int).Lsh(big.NewInt(1), 128)
	return rand.Int(rand.Reader, serialNumberLimit)
}

func createCertificateTemplate(info *Info) (*x509.Certificate, error) {
	certValidEnd := info.StartTime.Add(info.ValidDuration)
	serialNumber, err := newSerialNumber()
	if err != nil {
		return nil, err
	}
	certName := info.toPkix()
	template := &x509.Certificate{
		SerialNumber:          serialNumber,
		Subject:               certName,
		Issuer:                certName,
		NotBefore:             info.StartTime,
		NotAfter:              certValidEnd,
		KeyUsage:              x509.KeyUsageKeyEncipherment | x509.KeyUsageDigitalSignature,
		ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth},
		BasicConstraintsValid: true,
	}
	setHostsOnCertificate(template, info.Hosts)

	return template, nil
}

func setHostsOnCertificate(cert *x509.Certificate, hostCsv string) {
	hosts := strings.Split(hostCsv, ",")
	for _, h := range hosts {
		if ip := net.ParseIP(h); ip != nil {
			cert.IPAddresses = append(cert.IPAddresses, ip)
		} else {
			cert.DNSNames = append(cert.DNSNames, h)
		}
	}
}

func newCertificatePemBlock(certPemBytes []byte) *pem.Block {
	return &pem.Block{
		Type:  kPublicCertificatePemHeader,
		Bytes: certPemBytes,
	}
}

func newCertificateKeyPair(certPem *pem.Block, privateKeyPem *pem.Block) *CertificateKeyPair {
	return &CertificateKeyPair{
		X509PEMBytes:       pem.EncodeToMemory(certPem),
		PrivateKeyPEMBytes: pem.EncodeToMemory(privateKeyPem),
	}
}

func fileExists(path string) bool {
	_, err := os.Stat(path)
	if err == nil {
		return true
	}
	if os.IsNotExist(err) {
		return false
	}
	return false
}

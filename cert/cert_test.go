package cert

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

const FAKE_X509_CERTIFICATE = "x509"
const FAKE_PRIVATE_KEY = "private-key"

func TestGetCertificatePEM(t *testing.T) {
	assert := assert.New(t)
	assert.Equal(FAKE_X509_CERTIFICATE, newCertificateKeyPairForTest().GetCertificatePEM())
}

func TestGetPrivateKeyPEM(t *testing.T) {
	assert := assert.New(t)
	assert.Equal(FAKE_PRIVATE_KEY, newCertificateKeyPairForTest().GetPrivateKeyPEM())
}

func newCertificateKeyPairForTest() *CertificateKeyPair {
	return &CertificateKeyPair{
		X509PEMBytes:       []byte("x509"),
		PrivateKeyPEMBytes: []byte("private-key"),
	}
}

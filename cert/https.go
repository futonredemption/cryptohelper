package cert

import (
	"crypto/rand"
	"crypto/x509"
)

func AsHttpsCertificate(info *Info) (*CertificateKeyPair, error) {
	privateKey, err := createPrivateKey(info.kd)
	if err != nil {
		return nil, err
	}

	template, err := createCertificateTemplate(info)
	if err != nil {
		return nil, err
	}

	derBytes, err := x509.CreateCertificate(rand.Reader, template, template, publicKey(privateKey), privateKey)
	if err != nil {
		return nil, err
	}

	pemPriv, err := pemBlockForKey(privateKey)
	if err != nil {
		return nil, err
	}

	return newCertificateKeyPair(newCertificatePemBlock(derBytes), pemPriv), nil
}

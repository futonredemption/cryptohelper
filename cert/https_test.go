package cert

import (
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
)

func ExampleAsHttpsCertificate() {
	dir, err := ioutil.TempDir("", "certtest")
	if err != nil {
		log.Fatal(err)
	}

	defer os.RemoveAll(dir) // clean up
	pair, err := AsHttpsCertificate(NewInfo())
	if err != nil {
		log.Fatal(err)
	}
	pair.Write(filepath.Join(dir, "cert.pem"), filepath.Join(dir, "key.pem"))
}
